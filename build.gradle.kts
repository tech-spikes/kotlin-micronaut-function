import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import com.github.jengelman.gradle.plugins.shadow.transformers.Log4j2PluginsCacheFileTransformer
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    id("com.github.johnrengelman.shadow") version Version.Build.shadow
    id("io.spring.dependency-management") version Version.Build.dependencyManagement
    id("org.jetbrains.kotlin.jvm") version Version.kotlin
    id("org.jetbrains.kotlin.kapt") version Version.kotlin
    id("org.jetbrains.kotlin.plugin.allopen") version Version.kotlin
}

version = "0.1.0"
group = "co.makery"

repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
}

dependencyManagement {
    imports {
        mavenBom("io.micronaut:micronaut-bom:${Version.micronaut}")
    }
}

dependencies {
    compile("com.fasterxml.jackson.module:jackson-module-kotlin:${Version.jackson}")
    compile("io.micronaut:micronaut-function-aws")
    compile("io.micronaut:micronaut-inject-java")
    compile("io.micronaut:micronaut-runtime")
    compile("org.jetbrains.kotlin:kotlin-reflect")
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    kapt("io.micronaut:micronaut-inject-java")
    runtime("ch.qos.logback:logback-classic:${Version.logback}")
    runtime("com.amazonaws:aws-lambda-java-log4j2:${Version.awsLog4j}")
    runtime("io.micronaut:micronaut-function-web")
    runtime("io.micronaut:micronaut-http-server-netty")
    runtime("org.apache.logging.log4j:log4j-slf4j-impl:${Version.log4j}")
}

val run by tasks.getting(JavaExec::class) { jvmArgs("-noverify", "-XX:TieredStopAtLevel=1", "-Xmx25M") }

val shadowJar by tasks.getting(ShadowJar::class) {
    manifest {
        attributes("Main-Class" to "io.micronaut.function.executor.FunctionApplication")
    }
    mergeServiceFiles()
    transform(Log4j2PluginsCacheFileTransformer::class.java)
}

val compileKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
        javaParameters = true
    }
}

val compileTestKotlin by tasks.getting(KotlinCompile::class) {
    kotlinOptions {
        jvmTarget = "1.8"
        javaParameters = true
    }
}

allOpen { annotation("io.micronaut.aop.Around") }

application { mainClassName = "co.makery.Application" }
