object Version {
    const val awsLog4j = "1.0.0"
    const val jackson = "2.9.8"
    const val kotlin = "1.3.21"
    const val log4j = "2.9.1"
    const val logback = "1.2.3"
    const val micronaut = "1.0.4"

    object Build {
        const val dependencyManagement = "1.0.6.RELEASE"
        const val shadow = "4.0.4"
    }
}
