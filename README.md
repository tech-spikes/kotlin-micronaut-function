# Example Micronaut & AWS Lambda integration

This project contains a Hello World Micronaut application configured with AWS Lambda integration. It is based on the [official 'Deploying Functions to AWS Lambda' integration guide](https://docs.micronaut.io/latest/guide/index.html#lambdaFunctions).

## Build & Run

You can run the application as a standard Micronaut application with `./gradlew run`. By convention, every serverless function will be exposed as a controller. The bean `@FunctionBean("functional-greeting")` will be mapped a URL `http://localhost:8080/functional-greeting`.

Before deploying the application, you have to use the `./gradlew shadowJar` command. The `shadow` Gradle plugin has some custom configuration:

```
val shadowJar by tasks.getting(ShadowJar::class) {
    manifest {
        attributes("Main-Class" to "io.micronaut.function.executor.FunctionApplication")
    }
    mergeServiceFiles()
    transform(Log4j2PluginsCacheFileTransformer::class.java)
}
```

Please note, that we change the `Main-Class` to `io.micronaut.function.executor.FunctionApplication`. You can upload the resulting JAR file directly to AWS Lambda.