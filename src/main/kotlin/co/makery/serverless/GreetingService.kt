package co.makery.serverless

import javax.inject.Singleton

@Singleton
class GreetingService {

    fun greet(request: GreetingRequest): GreetingResponse = GreetingResponse("Hello ${request.name}!")
}
