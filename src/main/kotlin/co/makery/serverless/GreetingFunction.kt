package co.makery.serverless

import io.micronaut.function.FunctionBean
import java.util.function.Function
import javax.inject.Inject

data class GreetingRequest(val name: String)

data class GreetingResponse(val message: String)

@FunctionBean("functional-greeting")
class GreetingFunction : Function<GreetingRequest, GreetingResponse> {

    @Inject
    lateinit var greetingService: GreetingService

    override fun apply(request: GreetingRequest): GreetingResponse = greetingService.greet(request)

    // http -v localhost:8080/functional-greeting name=Makers
}
