package co.makery.serverless

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Replaces
import io.micronaut.jackson.JacksonConfiguration
import io.micronaut.jackson.ObjectMapperFactory
import javax.inject.Singleton

@Factory
@Replaces(ObjectMapperFactory::class)
class KotlinObjectMapperFactory : ObjectMapperFactory() {

    @Singleton
    @Replaces(ObjectMapper::class)
    override fun objectMapper(jacksonConfiguration: JacksonConfiguration?, jsonFactory: JsonFactory?): ObjectMapper {

        return super.objectMapper(jacksonConfiguration, jsonFactory).apply {
            registerModule(KotlinModule())
            enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)
        }
    }
}
